let arr = [23, '23',25,76,'привіт','hello', true, 57, true, false,];
let typeOfInput = 'boolean';// я не робив це через prompt, бо це не обов'язково було робити. Достатньо ввести у цей аргумент потрібний тип даних.
function filterBy(arr, typeOfInput){
    let result = arr.filter(function(item){
       if (typeof item !== typeOfInput){
        item = String(item); // зробив це, бо ця функція повертає тільки значення true. Коли вона намагається повернути false, то просто виключає його.
        return item;
       }
       
    }) 
    return result;
}
alert(filterBy(arr, typeOfInput));
console.log(filterBy(arr, typeOfInput));

/* 
1. forEach потрібен тільки для перебору даних масиву та, наприклад, виводу їх на у консоль. Він нічого не повертає.
2.Можна використати метод splice. Також є методи pop та shift. pop видаляє з кінця елемент масиву, shift з початку.
3. Можна використати метод Array.isArray(). Поверне true якщо це масив.

*/